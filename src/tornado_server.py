#!/usr/bin/env python

import os
import sys

from datetime import datetime

import django
from django.conf import settings
from django.core.management.base import OutputWrapper
from django.utils import autoreload, encoding, six

from tornado import httpserver, ioloop, options, web, wsgi

from project.sockets import socket_routers
from project.wsgi import application


options.define('autoreload', type=bool, default=False)
options.define('stdout', default=sys.stdout)
options.define('shutdown_message', type=str, default='\nShutting down...')


class ApplicationServer(object):

    def __init__(self):
        address = getattr(settings, "LISTEN_ADDRESS", "0.0.0.0")
        self.port = os.environ.get('PORT', 8000)
        self.addr = getattr(settings, "APPLICATION_ADDRESS", address)
        self.host = getattr(settings, "APPLICATION_HOST", '')
        self.stdout = OutputWrapper(options.options.stdout)

    def get_server(self):

        # define handlers
        handlers = []

        # sockets
        for router in socket_routers:
            handlers += router.urls

        # django app
        wsgi_app = wsgi.WSGIContainer(application)

        handlers += [(r'.*', web.FallbackHandler, dict(fallback=wsgi_app))]

        tornado_app = web.Application(handlers)
        server = httpserver.HTTPServer(tornado_app)

        return server

    def run(self):

        self.get_server().listen(self.port, self.addr)

        io_loop = ioloop.IOLoop.instance()

        shutdown_message = options.options.shutdown_message
        quit_command = 'CTRL-BREAK' if sys.platform == 'win32' else 'CONTROL-C'

        now = datetime.now().strftime('%B %d, %Y - %X')
        if six.PY2:
            now = now.decode(encoding.get_system_encoding())

        self.stdout.write((
            "%(started_at)s\n"
            "Django version %(version)s, using settings %(settings)r\n"
            "Starting tornado server at http://%(addr)s:%(port)s/\n"
            "Quit the server with %(quit_command)s.\n"
        ) % {
            "started_at": now,
            "version": django.get_version(),
            "settings": settings.SETTINGS_MODULE,
            "addr": self.addr,
            "port": self.port,
            "quit_command": quit_command
        })

        try:
            io_loop.start()
        except KeyboardInterrupt:
            if shutdown_message:
                self.stdout.write(shutdown_message)
            sys.exit(0)
        except Exception:
            raise


def main():
    options.parse_command_line()

    server = ApplicationServer()

    if options.options.autoreload:
        autoreload.main(server.run)
    else:
        server.run()

if __name__ == '__main__':
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "project.settings")
    main()

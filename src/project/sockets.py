
from sockjs.tornado import SockJSRouter

from matchplan.sockets import EventConnection, GameConnection


socket_routers = [
    SockJSRouter(EventConnection, '/sockets/events'),
    SockJSRouter(GameConnection, '/sockets/games')
]

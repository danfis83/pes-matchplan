
from .settings import *

DEBUG = False
TEMPLATE_DEBUG = False

ALLOWED_HOSTS = ['pes-matchplan.herokuapp.com']

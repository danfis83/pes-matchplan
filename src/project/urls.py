
from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic.base import TemplateView

from .api import router as api_router
from .views import cached_javascript_catalog


urlpatterns = patterns(
    '',
    url(r'^admin/', include(admin.site.urls)),

    (r'^i18n/', include('django.conf.urls.i18n')),
    (r'^jsi18n/$', cached_javascript_catalog, dict(packages=('project',))),

    url(r'^accounts/', include('accounts.urls')),

    url(r'^$', TemplateView.as_view(template_name='base.html'), name='home')
)

# api
urlpatterns += patterns(
    '',
    url(r'^api/auth/',
        include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api/', include(api_router.urls))
)

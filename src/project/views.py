
from django.utils import timezone
from django.views.decorators.http import last_modified
from django.views.i18n import javascript_catalog


@last_modified(lambda req, **kw: timezone.now())
def cached_javascript_catalog(request, domain='djangojs', packages=None):
    return javascript_catalog(request, domain, packages)

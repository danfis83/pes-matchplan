
from rest_framework import routers

from matchplan.api import (
    EventViewSet,
    GameViewSet,
    PlayerViewSet,
    UserViewSet,
    LeagueViewSet,
    TeamViewSet
)


router = routers.DefaultRouter()
router.register(r'events', EventViewSet)
router.register(r'players', PlayerViewSet)
router.register(r'games', GameViewSet)
router.register(r'users', UserViewSet)

router.register(r'leagues', LeagueViewSet)
router.register(r'teams', TeamViewSet)

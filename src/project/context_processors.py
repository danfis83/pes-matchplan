
import os

from django.conf import settings
from django.utils.version import get_version


def django_version(request):
    return {"DJANGO_VERSION": get_version()}


def _get_staticfile_timestamp(dirname):
    mtime = 0
    for staticdir in settings.STATICFILES_DIRS:
        dirpath = os.path.join(staticdir, dirname)
        if not os.path.exists(dirpath):
            continue
        for filename in os.listdir(dirpath):
            filepath = os.path.join(dirpath, filename)
            mtime = max(mtime, int(os.path.getmtime(filepath)))
    return mtime


def css_timestamp(request):
    # get timestamp of css files
    return {'CSS_TIMESTAMP': _get_staticfile_timestamp('css')}


def js_timestamp(request):
    # get timestamp of js files
    return {'JS_TIMESTAMP': _get_staticfile_timestamp('js')}

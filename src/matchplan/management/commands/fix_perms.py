
from django.core.management import BaseCommand

from guardian.shortcuts import assign_perm

from matchplan.models import Event, Game, Player


class Command(BaseCommand):

    def handle(self, *args, **options):
        event_owner_perms = ['view', 'change', 'delete']
        event_player_perms = ['view']
        player_perms = ['view']
        game_event_owner_perms = ['view', 'change', 'delete']

        for event in Event.objects.all():
            # owner permissions
            for perm in event_owner_perms:
                codename = '{0}.{1}_{2}'.format(event._meta.app_label, perm,
                                                event._meta.model_name)
                if not event.owner.has_perm(codename, event):
                    assign_perm(codename, event.owner, event)

            # player event permissions
            for perm in event_player_perms:
                codename = '{0}.{1}_{2}'.format(event._meta.app_label, perm,
                                                event._meta.model_name)
                for player in event.players.all():
                    if not player.user.has_perm(codename, event):
                        assign_perm(codename, player.user, event)

            # player permissions (view other players)
            for perm in player_perms:
                codename = '{0}.{1}_{2}'.format(Player._meta.app_label, perm,
                                                Player._meta.model_name)
                for player in event.players.all():
                    for _player in event.players.all():
                        if not player.user.has_perm(codename, _player):
                            assign_perm(codename, player.user, _player)

        for game in Game.objects.all():
            # owner permissions
            for perm in game_event_owner_perms:
                codename = '{0}.{1}_{2}'.format(game._meta.app_label, perm,
                                                game._meta.model_name)
                if not game.event.owner.has_perm(codename, game):
                    assign_perm(codename, game.event.owner, game)

            # player permissions
            for perm in event_player_perms:
                codename = '{0}.{1}_{2}'.format(game._meta.app_label, perm,
                                                game._meta.model_name)
                for player in game.event.players.all():
                    if not player.user.has_perm(codename, game):
                        assign_perm(codename, player.user, game)

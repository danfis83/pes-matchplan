# -*- coding: utf-8 -*-

from django.core.management import BaseCommand

import json
import requests

from BeautifulSoup import BeautifulSoup

from matchplan.models import League, GAME_DATABASE_CHOICES


class Command(BaseCommand):

    def handle(self, *args, **options):

        for choice, name in GAME_DATABASE_CHOICES:
            method = getattr(self, 'handle_{0}'.format(choice), None)
            if method is None:
                msg = 'Could not find handler for "{0}" ... skipping"'
                print(msg.format(choice))
                continue

            print('Processing {0}'.format(choice))

            method(*args, **options)

    def handle_pes(self, *args, **options):

        base_url = 'http://www.pesmaster.com'
        start_url = '{0}/pes-2015/'.format(base_url)
        start_res = requests.get(start_url)
        start_soup = BeautifulSoup(start_res.content)

        league_urls = []
        leagues_list = start_soup.find('ul', attrs={'class': 'leagues-list'})
        for league_link in leagues_list.findAll('a'):
            league_urls.append(league_link['href'])

        quicklinks = start_soup.find('ul', attrs={'class': 'quicklinks'})
        for league_link in quicklinks.findAll('a'):
            league_urls.append(league_link['href'])

        if options['verbosity'] > 0:
            print('Found {0} leagues'.format(len(league_urls)))

        for idx, league_url in enumerate(league_urls, start=1):

            if options['verbosity'] > 0:
                print('Processing league {0}/{1}'.format(
                    idx, len(league_urls)))

            url = '{0}{1}'.format(base_url, league_url)
            res = requests.get(url)
            soup = BeautifulSoup(res.content)

            league_name = soup.find(
                'h1', attrs={'class': 'top-header'}).text.strip()

            if options['verbosity'] > 0:
                print(u'League: {0}'.format(league_name))

            league_obj, created = League.objects.get_or_create(
                name=league_name, game_database='pes')

            if created and options['verbosity'] > 0:
                print(u'Created new league {0}'.format(league_name))

            team_trs = (soup.find('table', attrs={'id': 'search-result-table'})
                        .find('tbody').findAll('tr'))

            for team_tr in team_trs:
                name = team_tr.findAll('td')[0].find('a').text.strip()
                power = team_tr.findAll('td')[1].find('span').text.strip()

                team_obj, created = league_obj.teams.get_or_create(name=name)
                team_obj.power = int(power)
                team_obj.save()

                if created and options['verbosity'] > 0:
                    print(u'Created new team {0}'.format(name))

    def handle_fifa(self, *args, **options):

        base_url = 'https://www.easports.com/fifa/ultimate-team/api/fut'
        display_url = '{0}/display'.format(base_url)
        item_url = '{0}/item'.format(base_url)
        display_res = requests.get(display_url)
        display_json = display_res.json()

        league_sections = []
        for param in display_json.get('filterLogic', []):
            if param.get('searchParamName') == 'club':
                league_sections = param.get('sections')
                break

        if options['verbosity'] > 0:
            print('Found {0} leagues'.format(len(league_sections)))

        for idx, league_section in enumerate(league_sections, start=1):

            if options['verbosity'] > 0:
                print('Processing league {0}/{1}'.format(
                    idx, len(league_sections)))

            league_name = league_section.get('name')

            if options['verbosity'] > 0:
                print(league_name)

            league_obj, created = League.objects.get_or_create(
                name=league_name, game_database='fifa')

            if created and options['verbosity'] > 0:
                print(u'Created new league {0}'.format(league_name))

            for team_id in league_section.get('idList'):
                items = self._load_fifa_items(item_url, dict(club=team_id))

                if not items:
                    if options['verbosity'] > 0:
                        print('Error processing team id {0}'.format(team_id))
                    continue

                name = items[0].get('club', {}).get('name')

                if not name:
                    continue

                players = dict()
                for item in items:
                    item_id, rating = item.get('id'), item.get('rating')
                    players[item_id] = int(rating)

                power = float(sum(players.values())) / len(players)

                team_obj, created = league_obj.teams.get_or_create(name=name)
                team_obj.power = int(power)
                team_obj.save()

                if created and options['verbosity'] > 0:
                    print(u'Created new team {0}'.format(name))

    def _load_fifa_items(self, url, payload):
        """
        load all items (handle pagination)
        """
        items, page = [], 1
        while True:
            payload.update(page=page)
            res = requests.get(url, params=dict(
                jsonParamObject=json.dumps(payload)))

            if res.status_code == 200:
                items += res.json().get('items', [])

                if page < res.json().get('totalPages'):
                    page += 1
                else:
                    break

            else:
                # FIXME: show/handle error?
                return []

        return items

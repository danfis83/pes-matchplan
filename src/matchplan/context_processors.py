

from .models import GAME_DATABASE_CHOICES


def game_databases(request):
    databases = request.user.is_authenticated and GAME_DATABASE_CHOICES or ()
    return dict(GAME_DATABASES=databases)

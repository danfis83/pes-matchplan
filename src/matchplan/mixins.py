
class StatsModelMixin(object):

    def clear_stats(self, save=False):
        """
        unset (None) all stored stats
        """
        fields = getattr(self, 'stat_fields', [])
        for field in fields:
            setattr(self, field, None)

        if save:
            self.save()

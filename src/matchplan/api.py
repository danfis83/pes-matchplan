
from django.conf import settings
from django.contrib.auth import get_user_model
from django.db.models import Q, Max

from rest_framework import filters, serializers, status, viewsets
from rest_framework.response import Response

from .models import Event, Game, Player, League, Team
from .permissions import ExtendedObjectPermissions


User = get_user_model()


# user resource

class UserSerializer(serializers.ModelSerializer):

    user_name = serializers.CharField(read_only=True)
    initials = serializers.CharField(read_only=True)
    points = serializers.FloatField(read_only=True)
    scores_for = serializers.FloatField(read_only=True)
    scores_against = serializers.FloatField(read_only=True)
    wins = serializers.IntegerField(read_only=True)
    draws = serializers.IntegerField(read_only=True)
    losses = serializers.IntegerField(read_only=True)

    def to_representation(self, obj):

        data = super(UserSerializer, self).to_representation(obj)

        players_qs = obj.players.all()

        # username
        user_name = obj.get_full_name() or obj.username

        # initials
        # TODO: check for unique initials
        if obj.first_name and obj.last_name:
            initials = u"{0}{1}".format(obj.first_name[0], obj.last_name[0])
        else:
            initials = obj.username[:2]

        # points
        points = sum(p.points or 0 for p in players_qs)

        # events
        events = [p.event_id for p in players_qs]

        # games
        home_games = list(players_qs.values_list('games_home', flat=True))
        away_games = list(players_qs.values_list('games_away', flat=True))

        # scores
        scores_for = sum(p.scores_for or 0 for p in players_qs)
        scores_against = sum(p.scores_against or 0 for p in players_qs)

        # wins
        wins = sum(p.wins or 0 for p in players_qs)
        # draws
        draws = sum(p.draws or 0 for p in players_qs)
        # losses
        losses = sum(p.losses or 0 for p in players_qs)

        data.update(dict(
            user_name=user_name,
            initials=initials,
            points=points,
            games=(home_games + away_games),
            events=events,
            scores_for=scores_for,
            scores_against=scores_against,
            wins=wins,
            draws=draws,
            losses=losses
        ))

        return data

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'initials', 'points',
                  'scores_for', 'scores_against', 'user_name', 'id', 'wins',
                  'draws', 'losses')
        read_only_fields = ('first_name', 'last_name', 'id')


class UserViewSet(viewsets.ReadOnlyModelViewSet):

    queryset = (User.objects.filter(is_active=True)
                .exclude(id=settings.ANONYMOUS_USER_ID))
    serializer_class = UserSerializer


# player resource
class PlayerSerializer(serializers.ModelSerializer):

    user = UserSerializer(read_only=True)
    event_games = serializers.ListField(read_only=True)
    finished_games = serializers.IntegerField(read_only=True)
    i18n_status = serializers.CharField(read_only=True)

    class Meta:
        model = Player
        fields = ('id', 'event', 'number', 'status', 'user', 'points',
                  'scores_for', 'scores_against', 'wins', 'losses', 'draws',
                  'event_games', 'finished_games', 'i18n_status')
        read_only_fields = ('id',)

    def to_representation(self, obj):
        data = super(PlayerSerializer, self).to_representation(obj)

        # games
        data.update(dict(
            event_games=obj.games.values_list('pk', flat=True),
            finished_games=(
                obj.games.filter(status=Game.STATUS.finished).count())
        ))

        return data


class PlayerViewSet(viewsets.ModelViewSet):

    queryset = Player.objects.all()
    serializer_class = PlayerSerializer
    filter_fields = ('event',)
    filter_backends = (
        filters.DjangoObjectPermissionsFilter,
        filters.DjangoFilterBackend,
        filters.OrderingFilter,
        filters.SearchFilter
    )
    permission_classes = (ExtendedObjectPermissions,)

    def get_queryset(self, *args, **kwargs):
        """
        get players the user is related with
        (player in same events or owner of events)
        """
        qs = super(PlayerViewSet, self).get_queryset(*args, **kwargs)
        player_events = (qs.filter(
            user=self.request.user, status=Player.STATUS.active)
            .values_list('event_id', flat=True))
        qs = qs.filter(Q(event__in=player_events) |
                       Q(event__owner=self.request.user))
        return qs


# event resource
class EventSerializer(serializers.ModelSerializer):

    players = PlayerSerializer(many=True, read_only=True)
    event_games = serializers.ListField(read_only=True)
    url = serializers.URLField(read_only=True, source='get_absolute_url')
    i18n_status = serializers.CharField(read_only=True)

    class Meta:
        model = Event
        fields = ('id', 'title', 'power_factor_enabled', 'i18n_status',
                  'score_factor_enabled', 'points_win', 'points_draw',
                  'points_loss', 'start', 'end', 'status', 'players',
                  'event_games', 'url', 'owner', 'game_database')
        read_only_fields = ('status', 'id')

    def to_representation(self, obj):

        data = super(EventSerializer, self).to_representation(obj)

        # games
        data.update(dict(
            event_games=[g.pk for g in obj.games.all()]
        ))

        return data


class EventViewSet(viewsets.ModelViewSet):

    queryset = Event.objects.all()
    serializer_class = EventSerializer
    filter_backends = (
        filters.DjangoObjectPermissionsFilter,
        filters.DjangoFilterBackend,
        filters.OrderingFilter,
        filters.SearchFilter
    )
    permission_classes = (ExtendedObjectPermissions,)

    def get_queryset(self, *args, **kwargs):
        """
        get events for user where the user is (active) player or owner
        """
        qs = super(EventViewSet, self).get_queryset(*args, **kwargs)
        player_events = self.request.user.players.filter(
            status=Player.STATUS.active).values_list('event_id', flat=True)
        qs = qs.filter(Q(pk__in=player_events) | Q(owner=self.request.user))
        return qs

    def _handle_players(self, request, instance):
        players = request.data.get('players', [])
        instance_players = instance.players.all()

        # add players
        for player in players:
            user_id = player.get('user').get('id')
            number = int(player.get('number'))
            try:
                player_obj = instance_players.get(user_id=user_id)
                created = False
            except Player.DoesNotExist:
                player_obj = Player(event=instance, user_id=user_id)
                created = True

            if created or not player_obj.number == number:
                # check if number is already user by another player
                if instance_players.filter(number=number).exists():
                    existing = instance_players.get(number=number)
                    max_number = instance_players.aggregate(Max('number'))
                    existing.number = max_number.get('number__max') + 1
                    existing.save()

                player_obj.number = int(player.get('number'))
                player_obj.save()

        # remove players
        user_ids = [player.get('user').get('id') for player in players]
        to_remove = []
        for player in instance_players:
            if player.user_id not in user_ids:
                to_remove.append(player.id)
        if to_remove:
            instance_players.filter(pk__in=to_remove).delete()

    def create(self, request, *args, **kwargs):
        data = request.data
        data.update(dict(owner=request.user.pk))

        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        self._handle_players(request, serializer.instance)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED,
                        headers=headers)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        data = request.data
        data.update(dict(status=instance.get_status()))
        serializer = self.get_serializer(instance, data=request.data,
                                         partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        self._handle_players(request, serializer.instance)
        return Response(serializer.data)


# game resource
class GameSerializer(serializers.ModelSerializer):

    power_factor = serializers.FloatField(read_only=True)
    score_factor = serializers.FloatField(read_only=True)
    points_home = serializers.FloatField(read_only=True)
    points_away = serializers.FloatField(read_only=True)
    title = serializers.CharField(read_only=True)
    i18n_status = serializers.CharField(read_only=True)

    class Meta:
        model = Game
        fields = ('id', 'status', 'number', 'event', 'players_home',
                  'players_away', 'team_home', 'team_away', 'score_home',
                  'score_away', 'power_home', 'power_away', 'power_factor',
                  'score_factor', 'points_home', 'points_away', 'title',
                  'i18n_status')
        read_only_fields = ('id',)

    def to_representation(self, obj):

        data = super(GameSerializer, self).to_representation(obj)

        # games
        data.update(dict(
            title='{0}'.format(obj)
        ))

        return data


class GameFullSerializer(GameSerializer):

    players_home = PlayerSerializer(many=True)
    players_away = PlayerSerializer(many=True)
    event = EventSerializer()


class GameViewSet(viewsets.ModelViewSet):

    queryset = Game.objects.all()
    serializer_class = GameSerializer
    filter_fields = ('event', 'status')
    filter_backends = (
        filters.DjangoObjectPermissionsFilter,
        filters.DjangoFilterBackend,
        filters.OrderingFilter,
        filters.SearchFilter
    )
    permission_classes = (ExtendedObjectPermissions,)

    def get_serializer_class(self):
        assert self.serializer_class is not None, (
            "'%s' should either include a `serializer_class` attribute, "
            "or override the `get_serializer_class()` method."
            % self.__class__.__name__
        )

        if self.request.QUERY_PARAMS.get('full'):
            return GameFullSerializer

        return self.serializer_class

    def get_queryset(self, *args, **kwargs):

        qs = super(GameViewSet, self).get_queryset(*args, **kwargs)
        players = Player.objects.filter(status=Player.STATUS.active)

        if self.request.QUERY_PARAMS.get('for_user'):
            # get all games the user participated in
            players = players.filter(user=self.request.user)
            games = []
            for player in players:
                games += player.games.all().values_list('pk', flat=True)
            qs = qs.filter(pk__in=games)
        else:
            if self.request.QUERY_PARAMS.get('exclude_owner'):
                # get all games of user events (player only)
                qs = qs.filter(
                    event__in=players.values_list('event_id', flat=True))
            else:
                # get all games of user events (player or owner)
                qs = qs.filter(
                    Q(event__in=players.values_list('event_id', flat=True)) |
                    Q(event__owner=self.request.user)
                )

        return qs

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data, many=True)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED,
                        headers=headers)


class LeagueSerializer(serializers.ModelSerializer):

    class Meta:
        model = League
        fields = ('game_database', 'name', 'modified')


class LeagueViewSet(viewsets.ReadOnlyModelViewSet):

    queryset = League.active.all()
    serializer_class = LeagueSerializer
    filter_fields = ('game_database',)


class TeamSerializer(serializers.ModelSerializer):

    league = LeagueSerializer(read_only=True)
    power = serializers.FloatField(read_only=True)

    class Meta:
        model = Team
        fields = ('name', 'league', 'power', 'modified')


class TeamViewSet(viewsets.ReadOnlyModelViewSet):

    queryset = Team.active.all()
    serializer_class = TeamSerializer
    search_fields = ('name',)
    filter_fields = ('league__game_database',)

    def get_queryset(self, *args, **kwargs):
        qs = super(TeamViewSet, self).get_queryset(*args, **kwargs)
        return qs.filter(status=Team.STATUS.active)


from __future__ import print_function

import json
import sys

from importlib import import_module

from django.conf import settings
from django.contrib.auth import get_user, get_user_model
from django.core.exceptions import SuspiciousOperation
from django.test import RequestFactory

from sockjs.tornado import SockJSConnection

from .models import Event, Game


class Client(object):

    def __init__(self, user, connection):
        if not isinstance(user, get_user_model()):
            raise SuspiciousOperation("'user' is not a user object!")
        self.user = user
        self.connection = connection

    def __repr__(self):
        return self.user


class ClientConnection(SockJSConnection):

    clients = set()
    session_cookie_name = settings.SESSION_COOKIE_NAME
    model = None

    def __init__(self, *args, **kwargs):
        super(ClientConnection, self).__init__(*args, **kwargs)
        engine = import_module(settings.SESSION_ENGINE)
        self.SessionStore = engine.SessionStore
        if not self.model:
            raise Exception(
                'Undefined required attribute "model" in {0}'.format(
                    self.__class__.__name__))
        self.view_perms = '{0}.view_{1}'.format(
            self.model._meta.app_label, self.model._meta.model_name)

    def on_open(self, info):
        session_cookie = info.get_cookie(self.session_cookie_name)
        if session_cookie:
            session_key = session_cookie.value
            user = self._get_user_from_session_id(session_key)
            if user.is_authenticated():
                client = Client(user, self)
                self.clients.add(client)

    def on_close(self):
        for client in self.clients:
            if client.connection == self:
                self.clients.remove(client)

    def _get_user_from_session_id(self, session_key):
        # create fake request to use auth method
        request = RequestFactory().request()
        request.session = self.SessionStore(session_key)
        return get_user(request)

    def on_message(self, msg):
        connections = []
        for client in self.clients:
            # check if message is eligible for user
            if (not client.connection == self and
                    self._message_eligible_for_user(msg, client.user)):
                connections.append(client.connection)
        if settings.DEBUG:
            print('{0}Socket: {1}'.format(
                self.__class__.__name__, msg), file=sys.stdout)
        self.broadcast(connections, msg)

    def _message_eligible_for_user(self, msg, user):
        # check if user has permissions for object
        try:
            data = self._parse_json(msg)

            if data.get('action') == 'delete':
                # we don't really care if the user was able to see the event
                return True

            event = self.model.objects.get(pk=data.get('id'))
            return user.has_perm(self.view_perms, event)
        except Exception as ex:
            # TODO: handle exception? (at least log it!)
            print('{0}Socket: {1}'.format(
                self.__class__.__name__, ex), file=sys.stdout)
            return False

        return False

    def _parse_json(self, msg):
        """parse 'msg' via json, returns dict (empty if not parsable)"""
        try:
            return json.loads(msg)
        except:
            return {}


class EventConnection(ClientConnection):

    model = Event


class GameConnection(ClientConnection):

    model = Game

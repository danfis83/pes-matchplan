
from django.contrib.auth import get_user_model

from guardian.shortcuts import assign_perm, remove_perm


def pre_save_game(sender, **kwargs):
    instance = kwargs.get('instance')
    tracker = instance.tracker

    # recalculate power factor if powers have changed (or not set yet)
    if (not instance.power_factor or
            tracker.has_changed('power_home') or
            tracker.has_changed('power_away')):
        instance.power_factor = instance.get_power_factor()

    # recalculate score factor if scores have changed (or not set yet)
    if (not instance.score_factor or
            tracker.has_changed('score_home') or
            tracker.has_changed('score_away')):
        instance.score_factor = instance.get_score_factor()

    # recalculate points if scores, power or status has changed
    if (not instance.points_home or tracker.has_changed('score_home') or
            tracker.has_changed('score_away') or
            tracker.has_changed('power_home') or
            tracker.has_changed('status')):
        instance.points_home = instance.get_points_home()
    if (not instance.points_away or tracker.has_changed('score_away') or
            tracker.has_changed('score_home') or
            tracker.has_changed('power_away') or
            tracker.has_changed('status')):
        instance.points_away = instance.get_points_away()


def pre_save_player(sender, **kwargs):
    instance = kwargs.get('instance')

    if not instance.points:
        instance.points = instance.get_points()
    if not instance.points_per_game:
        instance.points_per_game = instance.get_points_per_game()
    if not instance.scores_for:
        instance.scores_for = instance.get_scores_for()
    if not instance.scores_for_per_game:
        instance.scores_for_per_game = instance.get_scores_for_per_game()
    if not instance.scores_against:
        instance.scores_against = instance.get_scores_against()
    if not instance.scores_against_per_game:
        instance.scores_against_per_game = (
            instance.get_scores_against_per_game())
    if not instance.wins:
        instance.wins = instance.get_wins()
    if not instance.draws:
        instance.draws = instance.get_draws()
    if not instance.losses:
        instance.losses = instance.get_losses()


def post_save_game(sender, **kwargs):
    instance = kwargs.get('instance')
    tracker = instance.tracker

    # event status
    event = instance.event
    event_games = event.games.all()
    if (event_games.filter(status=instance.STATUS.finished).count() ==
            event_games.count()
            and not event.status == event.STATUS.finished):
        # all matches are finished
        event.status = event.STATUS.finished
        event.save()
    elif (event_games.filter(status=instance.STATUS.finished).exists() and
            not event.status == event.STATUS.running):
        event.status = event.STATUS.running
        event.save()
    elif not event.status == event.STATUS.draft:
        event.status = event.STATUS.draft
        event.save()

    if (tracker.has_changed('score_factor') or
            tracker.has_changed('power_factor') or
            tracker.has_changed('points_home') or
            tracker.has_changed('points_away') or
            tracker.has_changed('score_home') or
            tracker.has_changed('score_away')):

        # TODO: check if this is not too expensive!
        players = (
            list(instance.players_home.all()) +
            list(instance.players_away.all())
        )
        for player in list(set(players)):
            player.clear_stats(save=True)

    # guardian permissions
    _post_save_game_permissions(**kwargs)


def _post_save_game_permissions(**kwargs):
    instance = kwargs.get('instance')
    # tracker = instance.tracker

    game_perms = ['view', 'change', 'delete']  # FIXME: check perms list

    for perm in game_perms:
        codename = '{0}.{1}_{2}'.format(instance._meta.app_label, perm,
                                        instance._meta.model_name)
        # TODO: check if this is too expensive
        for player in instance.event.players.all():
            if not player.user.has_perm(codename, instance):
                assign_perm(codename, player.user, instance)

        if not instance.event.owner.has_perm(codename, instance):
            assign_perm(codename, instance.event.owner, instance)


def post_save_event(sender, **kwargs):
    instance = kwargs.get('instance')
    tracker = instance.tracker

    # guardian permissions
    _post_save_event_owner_permissions(**kwargs)

    # if points_per_* or *_factor_enabled have changed, update all related data
    if (tracker.has_changed('power_factor_enabled') or
            tracker.has_changed('score_factor_enabled') or
            tracker.has_changed('points_win') or
            tracker.has_changed('points_draw') or
            tracker.has_changed('points_loss')):

        # need to get "clear" instance to prevent post_save recursion
        event = instance._meta.model.objects.get(pk=instance.pk)

        # update players
        for player in event.players.all():
            player.clear_stats(save=True)

        # update games
        for game in event.games.all():
            game.clear_stats(save=True)


def _post_save_event_owner_permissions(**kwargs):
    instance = kwargs.get('instance')
    tracker = instance.tracker

    if tracker.has_changed('owner_id'):
        # instance permissions
        event_perms = ['view', 'change', 'delete']  # owner

        previous_owner = None
        previous_owner_id = tracker.previous('owner_id')
        if previous_owner_id:
            previous_owner = get_user_model().objects.get(pk=previous_owner_id)

        for perm in event_perms:
            codename = '{0}.{1}_{2}'.format(instance._meta.app_label, perm,
                                            instance._meta.model_name)
            if not instance.owner.has_perm(codename, instance):
                assign_perm(codename, instance.owner, instance)

            # remove permissions from all other users
            if previous_owner and previous_owner.has_perm(codename, instance):
                remove_perm(codename, previous_owner, instance)

        # game permissions
        game_perms = ['view', 'change', 'delete']
        for game in instance.games.all():
            for perm in game_perms:
                codename = '{0}.{1}_{2}'.format(game._meta.app_label, perm,
                                                game._meta.model_name)
                if not instance.owner.has_perm(codename, game):
                    assign_perm(codename, instance.owner, game)


def post_save_player(sender, **kwargs):
    instance = kwargs.get('instance')
    tracker = instance.tracker
    event = instance.event

    if tracker.has_changed('event_id'):
        # event permissions
        event_perms = ['view']

        previous_event = None
        previous_event_id = tracker.previous('event_id')
        if previous_event_id:
            previous_event = instance.event._meta.model.objects.get(
                pk=previous_event_id)

        for perm in event_perms:
            codename = '{0}.{1}_{2}'.format(event._meta.app_label, perm,
                                            event._meta.model_name)
            if not instance.user.has_perm(codename, event):
                assign_perm(codename, instance.user, event)

            # event and player are unique_together
            if (previous_event and
                    instance.user.has_perm(codename, previous_event) and
                    not instance.user == previous_event.owner):
                remove_perm(codename, instance.user, previous_event)

        # player permissions (view other players)
        player_perms = ['view']
        for perm in player_perms:
            codename = '{0}.{1}_{2}'.format(
                event.players.model._meta.app_label, perm,
                event.players.model._meta.model_name)
            for player in event.players.all():
                for _player in event.players.all():
                    if not player.user.has_perm(codename, _player):
                        assign_perm(codename, player.user, _player)

        game_perms = ['view', 'change', 'delete']  # FIXME: check perms list
        for game in event.games.all():
            for perm in game_perms:
                codename = '{0}.{1}_{2}'.format(game._meta.app_label, perm,
                                                game._meta.model_name)
                if not instance.user.has_perm(codename, game):
                    assign_perm(codename, instance.user, game)

            # remove permission from previous event games
            if (previous_event and
                    not instance.user == previous_event.owner):
                for game in previous_event.games.all():
                    if instance.user.has_perm(codename, game):
                        remove_perm(codename, instance.user, game)


def post_delete_game(sender, **kwargs):
    # recalc numbers
    instance = kwargs.get('instance')
    for idx, game in enumerate(instance.event.games.all().order_by('number')):
        game.number = idx + 1
        game.save()

    # clear player stats
    home_players = instance.players_home.all().values_list('pk', flat=True)
    away_players = instance.players_away.all().values_list('pk', flat=True)
    players = instance.event.players.filter(
        pk__in=list(home_players) + list(away_players))
    for player in players:
        player.clear_stats(save=True)


def players_changed_game(sender, **kwargs):
    # players have changed on event ... reset stats for those
    instance = kwargs.get('instance')
    action = kwargs.get('action')
    model = kwargs.get('model')
    pk_set = kwargs.get('pk_set')

    if pk_set and action in ['post_add', 'post_remove']:
        for player in model.objects.filter(pk__in=pk_set):
            player.clear_stats(save=True)
    elif pk_set and 'clear' in action:
        # FIXME: does this even trigger?
        for player in instance.players_home.all():
            player.clear_stats(save=True)
        for player in instance.players_away.all():
            player.clear_stats(save=True)


# guardian view permissions
from django.db.models.signals import post_syncdb
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import Permission


def add_view_permissions(sender, **kwargs):
    """
    This syncdb hooks takes care of adding a view permission too all our
    content types.
    """
    # for each of our content types
    for content_type in ContentType.objects.all():
        # build our permission slug
        codename = "view_%s" % content_type.model

        # if it doesn't exist..
        qs = Permission.objects.filter(
            content_type=content_type, codename=codename)
        if not qs.exists():
            # add it
            Permission.objects.create(content_type=content_type,
                                      codename=codename,
                                      name="Can view %s" % content_type.name)
            print("Added view permission for {0}".format(content_type.name))

# check for all our view permissions after a syncdb
post_syncdb.connect(add_view_permissions)

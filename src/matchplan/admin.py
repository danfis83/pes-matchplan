# -*- coding: utf-8 -*-

from django.contrib import admin
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _, ugettext

from .models import Event, Game, Player, League, Team


class TranslatedStatusMixin(object):

    def _status(self, instance):
        return instance.i18n_status
    _status.short_description = _('status')


class PlayerInlineAdmin(admin.TabularInline):

    model = Player
    fields = ('user', 'number')
    extra = 1


class EventAdmin(TranslatedStatusMixin, admin.ModelAdmin):

    def _num_players(self, instance):
        return instance.players.count()
    _num_players.short_description = _('number of players')

    def _num_games(self, instance):
        return instance.games.count()
    _num_games.short_description = _('number of games')

    def _owner(self, instance):
        return instance.owner.get_full_name() or instance.owner
    _owner.short_description = _('owner')

    list_display = ('__unicode__', '_status', '_num_players', '_num_games',
                    '_owner', 'power_factor_enabled', 'score_factor_enabled',
                    'game_database', 'start')
    list_filter = ('start', 'status', 'power_factor_enabled',
                   'score_factor_enabled', 'owner')
    inlines = (PlayerInlineAdmin,)
    search_fields = ('title',)
    list_editable = ('power_factor_enabled', 'score_factor_enabled')
    fieldsets = (
        (None, {
            'fields': (('title', 'owner', 'game_database'),)
        }),
        (_('factors'), {
            'fields': (('power_factor_enabled', 'score_factor_enabled'),)
        }),
        (_('point settings'), {
            'fields': (('points_win', 'points_draw', 'points_loss'),)
        }),
        (_('status'), {
            'fields': (('status', 'status_changed'),)
        }),
        (_('timestamps'), {
            'fields': (('start', 'end'),)
        })
    )

admin.site.register(Event, EventAdmin)


class PlayerAdmin(TranslatedStatusMixin, admin.ModelAdmin):

    def _games(self, instance):
        return instance.games.count()
    _games.short_description = _('number of games (in event)')

    def _points(self, instance):
        return u"{} (ø{})".format(instance.points, instance.points_per_game)
    _points.short_description = _('points')

    def _scores_for(self, instance):
        return u"{} (ø{})".format(
            instance.scores_for, instance.scores_for_per_game)
    _scores_for.short_description = _('scores for')

    def _scores_against(self, instance):
        return u"{} (ø{})".format(
            instance.scores_against, instance.scores_against_per_game)
    _scores_against.short_description = _('scores against')

    def reset_stats(self, request, queryset):
        for obj in queryset:
            obj.clear_stats(save=True)
    reset_stats.short_description = _('reset statistics')

    list_display = ('user', 'event', 'number', '_status', '_games', '_points',
                    'wins', 'draws', 'losses', '_scores_for',
                    '_scores_against')
    list_filter = ('status', 'event')
    readonly_fields = ('status_changed',)
    actions = [reset_stats]

admin.site.register(Player, PlayerAdmin)


class GameAdmin(TranslatedStatusMixin, admin.ModelAdmin):

    def _score(self, instance):
        return u'{} : {}'.format(instance.score_home, instance.score_away)
    _score.short_description = _('score')

    list_display = ('__unicode__', 'event', 'number', '_status', '_score',
                    'power_factor', 'score_factor', 'points_home',
                    'points_away')
    list_filter = ('event', 'status')
    readonly_fields = ('status_changed',)
    fieldsets = (
        (None, {
            'fields': (('number', 'event'),)
        }),
        (_('players'), {
            'fields': (('players_home', 'players_away'),)
        }),
        (_('teams'), {
            'fields': (('team_home', 'power_home', 'score_home'),
                       ('team_away', 'power_away', 'score_away'))
        }),
        (_('status'), {
            'fields': (('status', 'status_changed'),)
        })
    )

admin.site.register(Game, GameAdmin)


class LeagueAdmin(admin.ModelAdmin):

    def _teams(self, instance):
        teams = instance.teams.count()
        admin_url = reverse('admin:matchplan_team_changelist')
        url = '{0}?league__id__exact={1}'.format(admin_url, instance.pk)
        link = '<a href="{0}">{1} {2}</a>'
        team_str = teams == 1 and 'team' or 'teams'
        return link.format(url, teams, ugettext(team_str))
    _teams.short_description = _('teams')
    _teams.allow_tags = True

    list_display = ('name', 'status', 'game_database', '_teams', 'modified')
    list_filter = ('status', 'game_database')

admin.site.register(League, LeagueAdmin)


class TeamAdmin(TranslatedStatusMixin, admin.ModelAdmin):

    def _game_database(self, instance):
        return instance.league.get_game_database_display()
    _game_database.short_description = _('game database')

    list_display = ('name', '_status', 'league', '_game_database', 'power',
                    'modified')
    list_filter = ('status', 'league__game_database', 'league')
    search_fields = ('name',)

admin.site.register(Team, TeamAdmin)

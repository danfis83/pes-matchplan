# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('matchplan', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='player',
            name='draws',
            field=models.PositiveSmallIntegerField(verbose_name='draws', null=True, editable=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='player',
            name='losses',
            field=models.PositiveSmallIntegerField(verbose_name='losses', null=True, editable=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='player',
            name='wins',
            field=models.PositiveSmallIntegerField(verbose_name='wins', null=True, editable=False),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='event',
            name='start',
            field=models.DateTimeField(default=datetime.datetime(2015, 1, 30, 10, 50, 53, 681911, tzinfo=utc), verbose_name='start'),
            preserve_default=True,
        ),
    ]

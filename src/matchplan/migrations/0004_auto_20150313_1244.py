# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('matchplan', '0003_auto_20150226_1417'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='start',
            field=models.DateTimeField(default=django.utils.timezone.now, verbose_name='start'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='game',
            unique_together=set([]),
        ),
    ]

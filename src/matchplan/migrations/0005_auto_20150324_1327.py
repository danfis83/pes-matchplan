# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('matchplan', '0004_auto_20150313_1244'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='game_database',
            field=models.CharField(default=b'', choices=[(b'pes', 'PES')], max_length=10, blank=True, help_text='Used for auto-suggestions', verbose_name='game database'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='league',
            name='game_database',
            field=models.CharField(default=b'pes', help_text='used for auto-suggestions', max_length=10, verbose_name='game database', choices=[(b'pes', 'PES')]),
            preserve_default=True,
        ),
    ]

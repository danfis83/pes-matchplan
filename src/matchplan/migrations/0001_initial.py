# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc
import model_utils.fields
import matchplan.mixins
import django.utils.timezone
from django.conf import settings
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, verbose_name='created', editable=False)),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, verbose_name='modified', editable=False)),
                ('status', model_utils.fields.StatusField(default=b'draft', max_length=100, verbose_name='status', no_check_for_status=True, choices=[(b'draft', b'draft'), (b'running', b'running'), (b'finished', b'finished')])),
                ('status_changed', model_utils.fields.MonitorField(default=django.utils.timezone.now, verbose_name='status changed', monitor='status')),
                ('title', models.CharField(max_length=100, verbose_name='title')),
                ('power_factor_enabled', models.BooleanField(default=True, help_text='$$_power_factor_help_text_$$', verbose_name='power factor?')),
                ('score_factor_enabled', models.BooleanField(default=True, help_text='$$_score_factor_help_text_', verbose_name='score/goal factor?')),
                ('points_win', models.PositiveSmallIntegerField(default=3, verbose_name='points for win', validators=[django.core.validators.MinValueValidator(1)])),
                ('points_draw', models.PositiveSmallIntegerField(default=1, verbose_name='points for draw/remis', validators=[django.core.validators.MinValueValidator(0)])),
                ('points_loss', models.PositiveSmallIntegerField(default=0, verbose_name='points for loss', validators=[django.core.validators.MinValueValidator(0)])),
                ('start', models.DateTimeField(default=datetime.datetime(2015, 1, 30, 9, 10, 18, 353759, tzinfo=utc), verbose_name='start')),
                ('end', models.DateTimeField(null=True, verbose_name='end', blank=True)),
                ('owner', models.ForeignKey(verbose_name='owner', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('-start', '-created'),
                'verbose_name': 'event',
                'verbose_name_plural': 'events',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Game',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, verbose_name='created', editable=False)),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, verbose_name='modified', editable=False)),
                ('status', model_utils.fields.StatusField(default=b'scheduled', max_length=100, verbose_name='status', no_check_for_status=True, choices=[(b'scheduled', b'scheduled'), (b'runnning', b'runnning'), (b'finished', b'finished')])),
                ('status_changed', model_utils.fields.MonitorField(default=django.utils.timezone.now, verbose_name='status changed', monitor='status')),
                ('number', models.PositiveSmallIntegerField(default=1, verbose_name='game number')),
                ('team_home', models.CharField(default=b'', max_length=100, verbose_name='team home', blank=True)),
                ('team_away', models.CharField(default=b'', max_length=100, verbose_name='team away', blank=True)),
                ('score_home', models.PositiveSmallIntegerField(default=0, verbose_name='score home', validators=[django.core.validators.MinValueValidator(0)])),
                ('score_away', models.PositiveSmallIntegerField(default=0, verbose_name='score away', validators=[django.core.validators.MinValueValidator(0)])),
                ('power_home', models.FloatField(blank=True, null=True, verbose_name='power home', validators=[django.core.validators.MinValueValidator(0)])),
                ('power_away', models.FloatField(blank=True, null=True, verbose_name='power away', validators=[django.core.validators.MinValueValidator(0)])),
                ('power_factor', models.FloatField(verbose_name='power factor', null=True, editable=False)),
                ('score_factor', models.FloatField(verbose_name='score factor', null=True, editable=False)),
                ('points_home', models.FloatField(verbose_name='points home', null=True, editable=False)),
                ('points_away', models.FloatField(verbose_name='points away', null=True, editable=False)),
                ('event', models.ForeignKey(related_name='games', verbose_name='event', to='matchplan.Event')),
            ],
            options={
                'ordering': ('-number', '-created'),
                'verbose_name': 'game',
                'verbose_name_plural': 'games',
            },
            bases=(matchplan.mixins.StatsModelMixin, models.Model),
        ),
        migrations.CreateModel(
            name='Player',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, verbose_name='created', editable=False)),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, verbose_name='modified', editable=False)),
                ('status', model_utils.fields.StatusField(default=b'active', max_length=100, verbose_name='status', no_check_for_status=True, choices=[(b'active', b'active'), (b'inactive', b'inactive')])),
                ('status_changed', model_utils.fields.MonitorField(default=django.utils.timezone.now, verbose_name='status changed', monitor='status')),
                ('number', models.PositiveSmallIntegerField(default=1, verbose_name='player number')),
                ('points', models.FloatField(verbose_name='points', null=True, editable=False)),
                ('points_per_game', models.FloatField(verbose_name='points_per_game', null=True, editable=False)),
                ('scores_for', models.PositiveSmallIntegerField(verbose_name='scores for', null=True, editable=False)),
                ('scores_against', models.PositiveSmallIntegerField(verbose_name='scores against', null=True, editable=False)),
                ('scores_for_per_game', models.FloatField(verbose_name='scores for per game', null=True, editable=False)),
                ('scores_against_per_game', models.FloatField(verbose_name='scores for per game', null=True, editable=False)),
                ('event', models.ForeignKey(related_name='players', to='matchplan.Event')),
                ('user', models.ForeignKey(related_name='players', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('number',),
                'verbose_name': 'player',
                'verbose_name_plural': 'players',
            },
            bases=(matchplan.mixins.StatsModelMixin, models.Model),
        ),
        migrations.AlterUniqueTogether(
            name='player',
            unique_together=set([('user', 'event'), ('event', 'number')]),
        ),
        migrations.AddField(
            model_name='game',
            name='players_away',
            field=models.ManyToManyField(related_name='games_away', verbose_name='away', to='matchplan.Player', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='game',
            name='players_home',
            field=models.ManyToManyField(related_name='games_home', verbose_name='home', to='matchplan.Player', blank=True),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='game',
            unique_together=set([('number', 'event')]),
        ),
    ]

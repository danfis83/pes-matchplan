
from __future__ import unicode_literals

from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.validators import MinValueValidator
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from model_utils import Choices, FieldTracker
from model_utils.models import (
    TimeStampedModel,
    StatusModel as UtilsStatusModel
)

from .mixins import StatsModelMixin


PRECISION = getattr(settings, 'POINTS_PRECISION', 2)

DEFAULT_GAME_DATABASE_CHOICES = (
    ('pes', _('PES')),
    ('fifa', _('FIFA'))
)

GAME_DATABASE_CHOICES = getattr(settings, 'GAME_DATABASE_CHOICES',
                                DEFAULT_GAME_DATABASE_CHOICES)


class StatusModel(UtilsStatusModel):

    def get_translated_status(self):
        return _(self.status)
    i18n_status = property(get_translated_status)

    class Meta:
        abstract = True


class Event(TimeStampedModel, StatusModel):
    """
    an event, duh!
    """

    STATUS = Choices('draft', 'running', 'finished')

    title = models.CharField(_('title'), max_length=100)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL,
                              verbose_name=_('owner'))

    power_factor_enabled = models.BooleanField(
        _('power factor?'),
        default=True,
        help_text=_('$$_power_factor_help_text_$$')
    )
    score_factor_enabled = models.BooleanField(
        _('score/goal factor?'),
        default=True,
        help_text=_('$$_score_factor_help_text_')
    )

    points_win = models.PositiveSmallIntegerField(
        _('points for win'), default=3, validators=[MinValueValidator(1)])
    points_draw = models.PositiveSmallIntegerField(
        _('points for draw/remis'), default=1,
        validators=[MinValueValidator(0)])
    points_loss = models.PositiveSmallIntegerField(
        _('points for loss'), default=0, validators=[MinValueValidator(0)])

    start = models.DateTimeField(_('start'), default=timezone.now)
    end = models.DateTimeField(_('end'), null=True, blank=True)

    game_database = models.CharField(
        _('game database'), max_length=10, choices=GAME_DATABASE_CHOICES,
        blank=True, default='', help_text=_('Used for auto-suggestions'))

    # tracker
    tracker = FieldTracker()

    class Meta:
        verbose_name = _('event')
        verbose_name_plural = _('events')
        ordering = ('-start', '-created')

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        # from django.core.urlresolvers import reverse
        # return reverse('event_detail', args=[self.pk])
        return '/#/events/{0}'.format(self.pk)

    def get_status(self):
        # event status
        event_games = self.games.all()
        status = self.status

        if (event_games.filter(status=Game.STATUS.finished).count() ==
                event_games.count() and
                not status == self.STATUS.finished):
            # all matches are finished
            status = self.STATUS.finished
        elif (event_games.filter(status=Game.STATUS.finished).exists() and
                not status == self.STATUS.running):
            status = self.STATUS.running
        elif not status == self.STATUS.draft:
            status = self.STATUS.draft

        return status

    def get_users(self, as_id_list=False):
        """
        get all users for event (owner + players)
        set 'as_objects' to recieve id/pk list instead of queryset
        """
        user_pks = [self.owner_id]
        players = self.players.filter(status=Player.STATUS.active)
        user_pks += players.values_list('user_id', flat=True)
        user_pks = set(user_pks)
        if as_id_list:
            return user_pks
        return get_user_model().objects.filter(pk__in=user_pks)

    users = property(get_users)


class Player(StatsModelMixin, TimeStampedModel, StatusModel):
    """
    player(s) for an event
    """

    STATUS = Choices('active', 'inactive')

    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='players',
                             verbose_name=_('user'))
    event = models.ForeignKey(Event, related_name='players',
                              verbose_name=_('event'))
    number = models.PositiveSmallIntegerField(_('player number'), default=1)

    # tracker
    tracker = FieldTracker()

    # stats
    points = models.FloatField(
        _('points'),
        editable=False,
        null=True
    )
    points_per_game = models.FloatField(
        _('points per game'),
        editable=False,
        null=True
    )
    scores_for = models.PositiveSmallIntegerField(
        _('scores for'),
        editable=False,
        null=True
    )
    scores_against = models.PositiveSmallIntegerField(
        _('scores against'),
        editable=False,
        null=True
    )
    scores_for_per_game = models.FloatField(
        _('scores for per game'),
        editable=False,
        null=True
    )
    scores_against_per_game = models.FloatField(
        _('scores for per game'),
        editable=False,
        null=True
    )
    wins = models.PositiveSmallIntegerField(
        _('wins'),
        editable=False,
        null=True
    )
    draws = models.PositiveSmallIntegerField(
        _('draws'),
        editable=False,
        null=True
    )
    losses = models.PositiveSmallIntegerField(
        _('losses'),
        editable=False,
        null=True
    )

    stat_fields = ('points', 'points_per_game', 'scores_for',
                   'scores_for_per_game', 'scores_against',
                   'scores_against_per_game', 'wins', 'draws', 'losses')

    class Meta:
        verbose_name = _('player')
        verbose_name_plural = _('players')
        ordering = ('number',)
        unique_together = (('user', 'event'), ('event', 'number'))

    def __unicode__(self):
        return u"{} - #{} - {}".format(self.user, self.number, self.event)

    def get_initials(self):
        """
        get initials or first two chars of username
        """
        if self.user.first_name and self.user.last_name:
            return u'{}{}'.format(self.user.first_name[0],
                                  self.user.last_name[0])
        return self.user.username[:2]

    initials = property(get_initials)

    def get_games(self):
        """
        get all games for this player
        """

        if not self.pk:
            return Game.objects.none()

        home_games = list(self.games_home.values_list('pk', flat=True))
        away_games = list(self.games_away.values_list('pk', flat=True))
        return Game.objects.filter(pk__in=home_games + away_games)

    games = property(get_games)

    def get_scores_for(self):
        """
        get all scores/goals for player
        """

        if not self.pk:
            return 0

        qs_home = self.games_home.filter(status=Game.STATUS.finished)
        qs_away = self.games_away.filter(status=Game.STATUS.finished)
        scores_home = qs_home.values_list('score_home', flat=True)
        scores_away = qs_away.values_list('score_away', flat=True)
        return sum(scores_home) + sum(scores_away)

    def get_scores_for_per_game(self):
        games = self.games.filter(status=Game.STATUS.finished).count()
        if games:
            scores_per_game = float(self.scores_for) / float(games)
        else:
            scores_per_game = 0
        return float(format(scores_per_game, '.{0}f'.format(PRECISION)))

    def get_scores_against(self):
        """
        get all scores/goals against player
        """

        if not self.pk:
            return 0

        qs_home = self.games_home.filter(status=Game.STATUS.finished)
        qs_away = self.games_away.filter(status=Game.STATUS.finished)
        against_home = qs_home.values_list('score_away', flat=True)
        against_away = qs_away.values_list('score_home', flat=True)
        return sum(against_home) + sum(against_away)

    def get_scores_against_per_game(self):
        games = self.games.filter(status=Game.STATUS.finished).count()
        if games:
            scores_per_game = float(self.scores_against) / float(games)
        else:
            scores_per_game = 0
        return float(format(scores_per_game, '.{0}f'.format(PRECISION)))

    def get_points(self, formatted=True):
        """
        get all points for player (for finised games)
        """
        points = 0
        for game in self.games.filter(status=Game.STATUS.finished):
            if self in game.players_home.all():
                points += game.points_home
            elif self in game.players_away.all():
                points += game.points_away

        if formatted:
            points = float(format(points, '.{0}f'.format(PRECISION)))

        return points

    def get_points_per_game(self):
        games = self.games.filter(status=Game.STATUS.finished).count()
        if games:
            points_per_game = self.get_points(formatted=False) / games
        else:
            points_per_game = 0
        return float(format(points_per_game, '.{0}f'.format(PRECISION)))

    def get_user_initials(self):
        if not self.user.first_name or not self.user.last_name:
            return u"P{0}".format(self.pk)
        # TODO: check for duplicates!
        return u"{0}{1}".format(
            self.user.first_name[0],
            self.user.last_name[0]
        ).upper()

    user_initials = property(get_user_initials)

    def get_wins(self):
        wins = 0
        for game in self.games.filter(status=Game.STATUS.finished):
            if (self in game.players_home.all() and
                    game.score_home > game.score_away):
                wins += 1
            elif (self in game.players_away.all() and
                    game.score_away > game.score_home):
                wins += 1
        return wins

    def get_draws(self):
        draws = 0
        for game in self.games.filter(status=Game.STATUS.finished):
            if game.score_home == game.score_away:
                draws += 1
        return draws

    def get_losses(self):
        losses = 0
        for game in self.games.filter(status=Game.STATUS.finished):
            if (self in game.players_home.all() and
                    game.score_home < game.score_away):
                losses += 1
            elif (self in game.players_away.all() and
                    game.score_away < game.score_home):
                losses += 1
        return losses


class Game(StatsModelMixin, TimeStampedModel, StatusModel):
    """
    one game of an event
    """

    STATUS = Choices('scheduled', 'running', 'finished')

    number = models.PositiveSmallIntegerField(_('game number'), default=1)
    event = models.ForeignKey(Event, verbose_name=_('event'),
                              related_name='games')

    players_home = models.ManyToManyField(Player, verbose_name=_('home'),
                                          related_name='games_home',
                                          blank=True)
    players_away = models.ManyToManyField(Player, verbose_name=_('away'),
                                          related_name='games_away',
                                          blank=True)

    team_home = models.CharField(_('team home'), max_length=100, blank=True,
                                 default='')
    team_away = models.CharField(_('team away'), max_length=100, blank=True,
                                 default='')

    score_home = models.PositiveSmallIntegerField(
        _('score home'), default=0, validators=[MinValueValidator(0)])
    score_away = models.PositiveSmallIntegerField(
        _('score away'), default=0, validators=[MinValueValidator(0)])

    # if power factor is enabled
    power_home = models.FloatField(_('power home'), blank=True, null=True,
                                   validators=[MinValueValidator(0)])
    power_away = models.FloatField(_('power away'), blank=True, null=True,
                                   validators=[MinValueValidator(0)])

    # tracker
    tracker = FieldTracker()

    # stats
    power_factor = models.FloatField(
        _('power factor'),
        editable=False,
        null=True
    )
    score_factor = models.FloatField(
        _('score factor'),
        editable=False,
        null=True
    )
    points_home = models.FloatField(
        _('points home'),
        editable=False,
        null=True
    )
    points_away = models.FloatField(
        _('points away'),
        editable=False,
        null=True
    )

    stat_fields = ('power_factor', 'score_factor', 'points_home',
                   'points_away')

    class Meta:
        verbose_name = _('game')
        verbose_name_plural = _('games')
        ordering = ('-number', '-created')
        # unique_together = (('number', 'event'),)

    def __unicode__(self):
        # FIXME: cache this?
        if self.team_home:
            team_home = u'({}) {}'.format(
                ','.join([str(p.number) for p in self.players_home.all()]),
                self.team_home
            )
        else:
            team_home = u','.join(
                [str(p.number) for p in self.players_home.all()])

        if self.team_away:
            team_away = u'{} ({})'.format(
                self.team_away,
                ','.join([str(p.number) for p in self.players_away.all()])
            )
        else:
            team_away = u','.join(
                [str(p.number) for p in self.players_away.all()])

        if self.status == self.STATUS.scheduled:
            return u'#{}: {} - {}'.format(self.number, team_home, team_away)

        return u'#{}: {} {} - {} {}'.format(
            self.number,
            team_home, self.score_home,
            self.score_away, team_away
        )

    def get_power_factor(self):
        """
        calculate power factor (if enabled on event)
        """
        if (not self.event.power_factor_enabled or not self.power_home or
                not self.power_away):
            return 1

        power_factor = (self.power_home > self.power_away and
                        self.power_home / self.power_away or
                        self.power_away / self.power_home)

        return float(format(power_factor, '.{0}f'.format(PRECISION)))

    def get_score_factor(self):
        """
        calculate score/goal factor (if enabled on event)
        """
        if not self.event.score_factor_enabled:
            return 1

        score_factor = (
            float(abs(self.score_home - self.score_away) - 1) / 10 + 1)
        return max(1, score_factor)

    def _get_points(self, team):
        """
        get points for team (home|away)
        """
        if team not in ['home', 'away']:
            return 0

        opp = team == 'home' and 'away' or 'home'

        points = self.event.points_loss

        if self.score_home == self.score_away:
            points = self.event.points_draw
        elif (getattr(self, 'score_{}'.format(team)) >
                (getattr(self, 'score_{}'.format(opp)))):
            points = self.event.points_win

        power_factor_enabled = (self.event.power_factor_enabled and
                                self.power_home and self.power_away)

        if (power_factor_enabled and getattr(self, 'power_{}'.format(team)) <
                getattr(self, 'power_{}'.format(opp))):
            points *= self.power_factor

        if self.event.score_factor_enabled:
            points *= self.score_factor

        return float(format(points, '.{0}f'.format(PRECISION)))

    def get_points_home(self):
        """
        get points for home players
        """
        return self._get_points(team='home')

    def get_points_away(self):
        """
        get points for away players
        """
        return self._get_points(team='away')


# teams, leagues

class League(TimeStampedModel, StatusModel):

    STATUS = Choices('active', 'inactive')

    game_database = models.CharField(
        _('game database'), max_length=10, choices=GAME_DATABASE_CHOICES,
        default='pes', help_text=_('Used for auto-suggestions'))
    name = models.CharField(_('name'), max_length=100)
    # TODO: logo?

    class Meta:
        verbose_name = _('league')
        verbose_name_plural = _('leagues')
        ordering = ('name',)

    def __unicode__(self):
        return self.name


class Team(TimeStampedModel, StatusModel):

    STATUS = Choices('active', 'inactive')

    league = models.ForeignKey(League, verbose_name=_('league'),
                               related_name='teams')
    name = models.CharField(_('name'), max_length=100)
    power = models.FloatField(_('power'), blank=True, null=True)

    class Meta:
        verbose_name = _('team')
        verbose_name_plural = _('teams')
        ordering = ('name',)

    def __unicode__(self):
        return self.name


# import signals
from .signals import (post_save_event, pre_save_game, post_save_game,
                      pre_save_player, post_save_player, post_delete_game,
                      players_changed_game)

# connect signals
models.signals.pre_save.connect(pre_save_game, sender=Game)
models.signals.pre_save.connect(pre_save_player, sender=Player)
models.signals.post_save.connect(post_save_event, sender=Event)
models.signals.post_save.connect(post_save_game, sender=Game)
models.signals.post_save.connect(post_save_player, sender=Player)
models.signals.post_delete.connect(post_delete_game, sender=Game)
models.signals.m2m_changed.connect(players_changed_game,
                                   sender=Game.players_home.through)
models.signals.m2m_changed.connect(players_changed_game,
                                   sender=Game.players_away.through)

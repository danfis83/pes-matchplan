
from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse_lazy
from django.http import Http404
from django.utils.translation import ugettext_lazy as _
from django.views.generic.base import TemplateView
from django.views.generic.edit import UpdateView

from .forms import AccountUpdateForm


class AccountIndexTemplateView(TemplateView):

    template_name = 'accounts/index.html'

    def get_context_data(self, **kwargs):
        context = super(AccountIndexTemplateView, self).get_context_data(
            **kwargs)
        context['page_title'] = _('Account')
        return context


class AccountUpdateView(UpdateView):

    model = get_user_model()
    form_class = AccountUpdateForm
    template_name = 'accounts/edit_form.html'
    success_url = reverse_lazy('account')

    def get_context_data(self, **kwargs):
        context = super(AccountUpdateView, self).get_context_data(**kwargs)
        context.update(dict(page_title=_('Edit data')))
        return context

    def get_object(self):
        if not self.request.user.is_authenticated():
            raise Http404()
        return self.request.user

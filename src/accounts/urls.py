
from django.conf.urls import patterns, url
from django.utils.translation import ugettext_lazy as _

from .views import AccountIndexTemplateView, AccountUpdateView


urlpatterns = patterns(
    'django.contrib.auth.views',

    url(r'^login/$',
        'login',
        dict(template_name='accounts/login.html',
             extra_context=dict(page_title=_('Log in'))),
        name='login'),
    url(r'^logout/$', 'logout_then_login', name='logout'),

    # password change
    url(r'^password/change/done/$',
        'password_change_done',
        dict(template_name='accounts/password_change_done.html',
             extra_context=dict(page_title=_('Password change done'))),
        name='password_change_done'),
    url(r'^password/change/$',
        'password_change',
        dict(template_name='accounts/password_change_form.html',
             extra_context=dict(page_title=_('Password change'))),
        name='password_change'),

    # password reset
    url(r'^password/reset/done/$',
        'password_reset_done',
        dict(template_name='accounts/password_reset_done.html',
             extra_context=dict(page_title=_('Password reset done'))),
        name='password_reset_done'),
    url(r'^password/reset/$',
        'password_reset',
        dict(template_name='accounts/password_reset_form.html',
             extra_context=dict(page_title=_('Password reset'))),
        name='password_reset'),
    url((r'^password/reset/(?P<uidb64>[0-9A-Za-z_\-]+)/'
         '(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$'),
        'password_reset_confirm',
        dict(template_name='accounts/password_reset_confirm.html',
             extra_context=dict(template_name=_('Password reset confirm'))),
        name='password_reset_confirm'),
    url(r'^password/reset/complete/$',
        'password_reset_complete',
        dict(template_name='accounts/password_reset_complete.html',
             extra_context=dict(page_title=_('Password reset done'))),
        name='password_reset_complete'),

    # edit personal data
    url(r'^edit/$', AccountUpdateView.as_view(), name='account_edit'),

    # account index
    url(r'^$', AccountIndexTemplateView.as_view(), name='account')
)


from django import forms
from django.contrib.auth import get_user_model


class AccountUpdateForm(forms.ModelForm):

    class Meta:
        model = get_user_model()
        fields = ('first_name', 'last_name', 'email')

    def __init__(self, *args, **kwargs):
        super(AccountUpdateForm, self).__init__(*args, **kwargs)
        for key, field in self.fields.items():
            field.required = not key == 'email' and True or False


var window, moment, toast, Scheduler, DEBUG, SockJS, gettext, user_id;


// global helper
var _display_error = function(error) {
  // display error via toast
  if (DEBUG) {
    console.log(error);
  }
  var msg = error.detail || 'Error';
  toast(msg, toast_duration);
};

// angular app

var app = angular.module('app', ['ngRoute', 'ngCookies']);
var toast_duration = 4000;

// Configs

app.run(function run($http, $cookies) {
  $http.defaults.headers.common['X-CSRFToken'] = $cookies.csrftoken;
});

app.config(function($routeProvider) {
  $routeProvider
    .when('/', {
      controller: 'DashboardCtrl',
      templateUrl: 'dashboard.html'
    })
    .when('/events/:id', {
      controller: 'EventCtrl',
      templateUrl: 'event_detail.html'
    })
    .when('/events', {
      controller: 'EventCtrl',
      templateUrl: 'event_list.html'
    })
    .otherwise({redirectTo: '/'});
});

app.constant('api_event_url', '/api/events/');
app.constant('api_user_url', '/api/users/');
app.constant('api_player_url', '/api/players/');
app.constant('api_game_url', '/api/games/');
app.constant('api_league_url', '/api/leagues/');
app.constant('api_team_url', '/api/teams/');


// Factories

app.service('SocketService', function() {

  var socket_url = '/sockets';
  var event_socket_url = socket_url + '/events';
  var game_socket_url = socket_url + '/games';

  var _event_socket;
  this.event_socket = function() {
    if (angular.isUndefined(_event_socket)) {
      _event_socket = new SockJS(event_socket_url);
    }
    return _event_socket;
  };

  var _game_socket;
  this.game_socket = function() {
    if (angular.isUndefined(_game_socket)) {
      _game_socket = new SockJS(game_socket_url);
    }
    return _game_socket;
  };


});


// Controllers

app.controller('DashboardCtrl',
  function($scope, $http, api_event_url, api_game_url, SocketService) {
    var events_url = api_event_url + '?ordering=-start&page_size=3';
    var games_url = api_game_url + '?ordering=-id&page_size=3&for_user=1';
    games_url += '&status=finished&full=1';

    $http.get(events_url).success(function(data) {
      $scope.events = (data && data.results) ? data.results : data || [];
      $scope.events_loaded = true;
    }).error(function(error) {
      _display_error(error);
      $scope.events_loaded = true;
    });

    $http.get(games_url).success(function(data) {
      $scope.games = (data && data.results) ? data.results : data || [];
      $scope.games_loaded = true;
    }).error(function(error) {
      _display_error(error);
      $scope.games_loaded = true;
    });

    // socket handling
    var event_socket = SocketService.event_socket();
    event_socket.onmessage = function(e) {
      var action, msg;
      var socket_data = JSON.parse(e.data);
      if (socket_data.content_type !== 'event') {
        return;
      }
      var updated, created, deleted;
      if (socket_data.action === 'delete') {
        angular.forEach($scope.events, function(event, idx) {
          if (event.id === socket_data.id) {
            deleted = event;
            $scope.events.splice(idx, 1);
          }
        });
        if (!angular.isUndefined(deleted)) {
          action = gettext('deleted') + ' ' + gettext('event');
          msg = action + ': ' + deleted.title;
          toast(msg, toast_duration);
          $scope.$apply();
        }
      } else {
        // fetch object
        $http.get(api_event_url + socket_data.id + '/')
          .success(function(data) {
            data.entrance = true;
            data.start = new Date(data.start);
            if (socket_data.action === 'update') {
              angular.forEach($scope.events, function(event, idx) {
                if (event.id === data.id) {
                  $scope.events[idx] = data;
                  updated = event;
                }
              });
            }
            if (socket_data.action === 'create' ||
                angular.isUndefined(updated)) {
              // add event
              $scope.events.push(data);
              updated = data;
              created = true;
            }

            if (!angular.isUndefined(updated)) {

              // notification
              action = gettext('new');
              if (socket_data.action === 'update' && !created) {
                action = gettext('updated');
              }
              msg = action + ': ' + gettext('event');
              msg += ' "' + data.title + '"';
              toast(msg, toast_duration);
            }
          });
      }

    };

    var game_socket = SocketService.game_socket();
    game_socket.onmessage = function(e) {
      // FIXME: DRY!
      var action, msg;
      var socket_data = JSON.parse(e.data);
      if (socket_data.content_type !== 'game') {
        return;
      }
      var updated, created, deleted;
      if (socket_data.action === 'delete') {
        angular.forEach($scope.games, function(game, idx) {
          if (game.id === socket_data.id) {
            deleted = game;
            $scope.games.splice(idx, 1);
          }
        });
        if (!angular.isUndefined(deleted)) {
          action = gettext('deleted') + ' ' + gettext('game');
          msg = action + ': ' + deleted.event.title + ' ' + deleted.title;
          toast(msg, toast_duration);
          $scope.$apply();
        }
      } else {
        // fetch object
        var game_url = api_game_url + socket_data.id + '/?full=1';
        game_url += '&for_user=1&status=finished';
        $http.get(game_url)
          .success(function(data) {
            data.entrance = true;
            if (socket_data.action === 'update') {
              angular.forEach($scope.games, function(game, idx) {
                if (game.id === data.id) {
                  $scope.games[idx] = data;
                  updated = game;
                }
              });
            }
            if (socket_data.action === 'create' ||
                angular.isUndefined(updated)) {
              // add event
              // check if user is in game
              var user_game;
              angular.forEach(data.players_home, function(player) {
                if (player.user.id === user_id) {
                  user_game = true;
                }
              });
              angular.forEach(data.players_away, function(player) {
                if (player.user.id === user_id) {
                  user_game = true;
                }
              });
              if (user_game && data) {
                $scope.games.push(data);
                updated = data;
                created = true;
              }
            }

            if (!angular.isUndefined(updated)) {

              // notification
              action = gettext('new');
              if (socket_data.action === 'update' && !created) {
                action = gettext('updated');
              }
              action += ': ' + gettext('game');
              msg = action + ' "' + data.event.title + ' ';
              msg += data.title + '"';
              toast(msg, toast_duration);
            }
          });
      }
    };
  });

app.controller('EventCtrl',
  function($scope, $http, $routeParams, api_event_url, api_game_url,
            api_user_url, api_player_url, api_team_url, SocketService) {

    // FIXME: this controller is way too long!

    // helpers

    // FIXME: need a directive here! ... this is not working properly!!!
    var _apply_sortable = function() {
      $('.sortable').sortable().bind('sortupdate', function() {

        angular.forEach($(this).children('li'), function(el, idx) {
          var user_id = parseInt($(el).data('id'), 10);

          angular.forEach($scope.event.players, function(player) {
            if (player.user.id === user_id) {
              player.number = idx + 1;
            }
          });
        });

        $scope.$apply();
      });
    };

    var _loadUsers = function() {
      $http.get(api_user_url).success(function(data) {
        if (data) {
          data = data.results || data;
          $scope.available_users = [];
          var event_users = [];
          angular.forEach($scope.event.players, function(player) {
            event_users.push(player.user.id);
          });
          angular.forEach(data, function(user) {
            if (event_users.indexOf(user.id) === -1) {
              $scope.available_users.push(user);
            }
          });
          _apply_sortable();
        }
      }).error(function(error) {
        _display_error(error);
      });
    };

    var _loadGames = function() {
      var games_url = api_game_url + '?event=' + $scope.event.id;
      $http.get(games_url).success(function(data) {
        $scope.games = data;
        $scope.games_loaded = true;
      }).error(function(error) {
        _display_error(error);
        $scope.games_loaded = true;
      });
    };

    var _loadPlayers = function() {
      if ($scope.event.players.length) {
        var players_url = api_player_url + '?event=' + $scope.event.id;
        players_url += '&page_size=' + $scope.event.players.length;
        $http.get(players_url).success(function(data) {
          if (data && data.results) {
            $scope.event.players = data.results;
          }
        });
      }
    };

    var _do_team_search = function(query) {
      var search_url = api_team_url + '?page_size=10';
      search_url += '&league__game_database=' + $scope.event.game_database;
      search_url += '&search=' + query;
      $http.get(search_url).success(function(data) {
        $scope.teams = (data && data.results) ? data.results : $scope.teams;
      });
      // FIXME: handle autocomplete errors?
    };

    var _get_next_number = function() {
      // get next (available) game number for event
      // FIXME: make number editable?
      var max = 0;
      angular.forEach($scope.games, function(game) {
        max = Math.max(game.number, max);
      });
      return max + 1;
    };

    // sockets
    var event_socket = SocketService.event_socket();
    event_socket.onmessage = function(e) {
      var action, msg;
      var socket_data = JSON.parse(e.data);
      if (socket_data.content_type !== 'event') {
        return;
      }
      var updated, deleted;
      var created = false;
      if (socket_data.action === 'delete') {
        if (!angular.isUndefined($scope.events)) {
          // list
          angular.forEach($scope.events, function(event, idx) {
            if (event.id === socket_data.id) {
              deleted = event;
              $scope.events.splice(idx, 1);
            }
          });
        } else if ($scope.event.id === socket_data.id) {
          deleted = $scope.event;
        }

        if (!angular.isUndefined(deleted)) {
          action = gettext('deleted') + ' ' + gettext('event');
          msg = action + ': ' + deleted.title;
          toast(msg, toast_duration);
          if ($scope.event && $scope.event.id === deleted.id) {
            window.location = '/#/events';
          } else {
            $scope.$apply();
          }
        }
      } else {
        // check if we are in a detail and if so, check if ids matches
        if ($scope.event && $scope.event.id !== socket_data.id) {
          // nope ... does not consern this event
          return;
        }
        // fetch object
        $http.get(api_event_url + socket_data.id + '/')
          .success(function(data) {
            data.entrance = true;
            data.start = new Date(data.start);
            if (socket_data.action === 'update') {
              angular.forEach($scope.events, function(event, idx) {
                if (event.id === data.id) {
                  $scope.events[idx] = data;
                  updated = event;
                }
              });
            }
            if (socket_data.action === 'create' ||
                angular.isUndefined(updated)) {
              // add event
              if ($scope.event.id === data.id) {
                $scope.event = data;
                updated = data;
              }
            }

            if (!angular.isUndefined(updated)) {

              // notification
              action = gettext('new');
              if (socket_data.action === 'update' && !created) {
                action = gettext('updated');
              }
              msg = action + ': ' + gettext('event') + ' "';
              msg += data.title + '"';
              toast(msg, toast_duration);

              // FIXME
              setTimeout(function() {
                // remove entrance to reactivate animation
                if ($scope.event.entrance) {
                  delete $scope.event.entrance;
                }
              }, 1500);
            }
          });
      }

    };

    var game_socket = SocketService.game_socket();
    game_socket.onmessage = function(e) {
      // FIXME: DRY!
      var action, msg;
      var socket_data = JSON.parse(e.data);
      if (socket_data.content_type !== 'game') {
        return;
      }
      var updated, created, deleted;
      if (socket_data.action === 'delete') {
        angular.forEach($scope.games, function(game, idx) {
          if (game.id === socket_data.id) {
            deleted = game;
            $scope.games.splice(idx, 1);
          }
        });
        if (!angular.isUndefined(deleted)) {
          action = gettext('deleted') + ' ' + gettext('game');
          msg = action + ': ' + deleted.title;
          toast(msg, toast_duration);
          $scope.$apply();
          _loadPlayers();
        }
      } else {
        // check if we are on event detail and if game is of that event
        if ($scope.event && $scope.event.id !== socket_data.event_id) {
          // nope ... does not consern this event
          return;
        }
        // fetch object
        $http.get(api_game_url + socket_data.id + '/')
          .success(function(data) {
            data.entrance = true;
            if (socket_data.action === 'update') {
              angular.forEach($scope.games, function(game, idx) {
                if (game.id === data.id) {
                  $scope.games[idx] = data;
                  updated = game;
                }
              });
            }
            if (socket_data.action === 'create' ||
                angular.isUndefined(updated)) {
              // add event
              if (data.event === $scope.event.id) {
                $scope.games.push(data);
                updated = data;
                created = true;
              }
            }

            if (!angular.isUndefined(updated)) {

              // notification
              action = gettext('new');
              if (socket_data.action === 'update' && !created) {
                action = gettext('updated');
              }
              action += ': ' + gettext('game');
              msg = action + ' "' + data.title + '"';
              toast(msg, toast_duration);
              _loadPlayers();
            }

            // FIXME
            setTimeout(function() {
              // remove entrance to reactivate animation
              angular.forEach($scope.games, function(game) {
                delete game.entrance;
              });
            }, 1500);
          });
      }
    };

    // init

    var url = api_event_url;
    $scope.user_id = user_id;

    if ($routeParams.id) {
      // detail

      $scope.games = [];

      url += $routeParams.id + '/';

      $http.get(url).success(function(data) {
        data.start = new Date(data.start);
        $scope.event = data;
        _loadUsers();
        _loadGames();
        $scope.modus = ($scope.event.players.length < 4) ? '1vs1' : '2vs2';
      }).error(function(error, code) {
        _display_error(error);
        if (code === 404) {
          window.location = '/#/events';
        }
      });

    } else {
      // list
      $scope.events = [];

      // event defaults
      var now = new Date();
      now.setMilliseconds(0);
      now.setSeconds(0);

      $scope.event = {
        players: [],
        power_factor_enabled: true,
        score_factor_enabled: true,
        points_win: 3,
        points_draw: 1,
        points_loss: 0,
        start: now
      };

      $scope.loadEvents = function(url) {
        $http.get(url).success(function(data) {
          if (data && data.results) {
            $scope.events = $scope.events.concat(data.results);
          }
          $scope.next = (data && data.next) ? data.next : null;
          _loadUsers();
          $scope.events_loaded = true;
        }).error(function(error) {
          _display_error(error);
          $scope.events_loaded = true;
        });
      };

      // init
      $scope.loadEvents(url + '?ordering=-start&page_size=5');
    }

    $scope.save = function() {

      if ($scope.syncing) {
        return;
      }

      $scope.syncing = true;

      var data = $scope.event;

      if ($scope.event.id) {
        // update
        $http.patch(url, data).success(function(data) {
          var socket_data = JSON.stringify({
            'action': 'update',
            'content_type': 'event',
            'id': data.id
          });
          event_socket.send(socket_data);
          data.start = new Date(data.start);
          $scope.event = data;
          $('#event-form').closeModal();
          $scope.syncing = false;
        }).error(function(error) {
          $scope.syncing = false;
          _display_error(error);
        });
      } else {
        // create
        $http.post(url, data).success(function(data) {
          event_socket.send(JSON.stringify({
            'action': 'create',
            'content_type': 'event',
            'id': data.id
          }));
          $scope.syncing = false;
          $('#event-form').closeModal();
          window.location = '/#/events/' + data.id + '/';
        }).error(function(error) {
          $scope.syncing = false;
          _display_error(error);
        });
      }
    };

    $scope.delete = function(el) {
      if ($scope.syncing) {
        return;
      }
      $scope.syncing = true;
      $http.delete(url).success(function() {
        var socket_data = JSON.stringify({
          'action': 'delete',
          'content_type': 'event',
          'id': $scope.event.id
        });
        event_socket.send(socket_data);
        $scope.syncing = false;
        $(el).closeModal();
        window.location = '/#/events/';
      }).error(function(error) {
        $scope.syncing = false;
        _display_error(error);
      });
    };

    $scope.addPlayer = function() {
      var id = $scope.new_player;
      $scope.new_player = null;
      var user_obj = null;
      var index = -1;
      angular.forEach($scope.available_users, function(user, idx) {
        if (user.id === parseInt(id, 10)) {
          user_obj = user;
          index = idx;
        }
      });
      if (user_obj && index > -1) {
        var player = {
          user: user_obj,
          number: $scope.event.players.length + 1
        };
        $scope.event.players = $scope.event.players.concat(player);
        // remove from available players
        $scope.available_users.splice(index, 1);
        // FIXME: use directive for dragging!
        setTimeout(_apply_sortable, 500);
      }
      $scope['event-form'].$setDirty(true);
    };

    $scope.removePlayer = function(player) {
      var index = $scope.event.players.indexOf(player);
      if (index > -1) {
        $scope.event.players.splice(index, 1);
        $scope.available_users = $scope.available_users.concat(player.user);

        // renumber $scope.event.players
        angular.forEach($scope.event.players, function(player, idx) {
          player.number = idx + 1;
        });
      }
      $scope['event-form'].$setDirty(true);
    };

    $scope.openModal = function(el) {
      $(el).openModal({
        dismissible: false  // TODO
      });
      _apply_sortable();

      // FIXME: this is just a very specific "workaround"
      //        ... find some generic solution for it
      // store current object to restore if modal is closed without save
      if (el === '#event-form') {
        var _clone = JSON.parse(JSON.stringify($scope.event));
        _clone.start = new Date(_clone.start);
        $scope._event = _clone;
      } else if (el === '#game-form' || el === '#score-form') {
        $scope._game = JSON.parse(JSON.stringify($scope.game));
      }
    };

    $scope.closeModal = function(el) {
      var form_name = $(el).find('form').attr('name');
      if (form_name && $scope[form_name].$dirty) {
        // try to restore object
        if ($scope._game && (form_name === 'game-form' ||
            form_name === 'score-form')) {
          $scope.game = $scope._game;
          angular.forEach($scope.games, function(game, idx) {
            if (game.id === $scope.game.id) {
              $scope.games[idx] = $scope.game;
            }
          });
        } else if ($scope._event && form_name === 'event-form') {
          $scope.event = $scope._event;
        }
      }
      $(el).closeModal();
    };

    // games
    $scope.new_game = {
      players_home: [],
      players_away: []
    };
    $scope.game = $scope.new_game;

    $scope.editGame = function(game) {
      $scope.game = game;
      $scope.openModal('#game-form');
    };

    $scope.addGamePlayer = function(team) {
      if (team === 'home') {
        $scope.game.players_home.push(parseInt($scope.new_home_player, 10));
        $scope.new_home_player = null;
      } else if (team === 'away') {
        $scope.game.players_away.push(parseInt($scope.new_away_player, 10));
        $scope.new_away_player = null;
      }
      $scope['game-form'].$setDirty(true);
    };

    $scope.removeGamePlayer = function(player) {
      var idx;
      if ($scope.game.players_home.indexOf(player.id) > -1) {
        idx = $scope.game.players_home.indexOf(player.id);
        $scope.game.players_home.splice(idx, 1);
      } else if ($scope.game.players_away.indexOf(player.id) > -1) {
        idx = $scope.game.players_away.indexOf(player.id);
        $scope.game.players_away.splice(idx, 1);
      }
      $scope['game-form'].$setDirty(true);
    };

    $scope.applyTeam = function(team) {
      if ($scope.event.power_factor_enabled && $scope.teams) {
        var team_name = $scope.game['team_' + team];
        angular.forEach($scope.teams, function(team_obj) {
          if (team_obj.name === team_name && team_obj.power) {
            $scope.game['power_' + team] = team_obj.power;
            return;
          }
        });
      }
    };

    $scope.searchTeam = function(query_var) {
      if ($scope.event.game_database) {
        var q = $scope.game[query_var];
        if (q) {
          q = q.toLowerCase();
          setTimeout(function() {
            var _q = $scope.game[query_var];
            if (_q && q === _q.toLowerCase() && q !== $scope.last_search) {
              _do_team_search(q);
              $scope.last_search = q;
            }
          }, 350);
        }
      }
    };

    $scope.saveGame = function() {

      if ($scope.syncing) {
        return;
      }

      $scope.syncing = true;

      var promise;

      var data = $scope.game;
      data.event = $scope.event.id;
      data.number = data.number || _get_next_number();

      var game_url = api_game_url;
      if ($scope.game.id) {
        // update
        game_url += $scope.game.id;
        promise = $http.patch(game_url, data);
      } else {
        // new (create)
        promise = $http.post(game_url, [data]);
      }

      promise.success(function(data) {
        // update data (event games, player games)
        $scope.syncing = false;
        $('#game-form').closeModal();
        var obj = angular.isArray(data) ? data[0] : data;
        var idx = -1;
        angular.forEach($scope.games, function(game, _idx) {
          if (game.id === obj.id) {
            idx = _idx;
          }
        });
        var action;
        if (idx === -1) {
          $scope.games.push(obj);
          action = 'create';
        } else {
          $scope.games[idx] = obj;
          action = 'update';
        }
        var socket_data = {
          'action': action,
          'content_type': 'game',
          'id': obj.id,
          'event_id': $scope.event.id
        };
        game_socket.send(JSON.stringify(socket_data));
        $scope.game = $scope.new_game;
      }).error(function(error) {
        $scope.syncing = false;
        _display_error(error);
      });
    };

    $scope.scoreGame = function(game) {
      $scope.game = game;
      $scope.status = game.status;
      $scope.openModal('#score-form');
    };

    $scope.saveScore = function() {
      if ($scope.syncing) {
        return;
      }
      $scope.syncing = true;

      var game_url = api_game_url + $scope.game.id + '/';
      $http.patch(game_url, $scope.game).success(function(data) {
        var socket_data = {
          'action': 'update',
          'content_type': 'game',
          'id': data.id,
          'event_id': $scope.event.id
        };
        game_socket.send(JSON.stringify(socket_data));
        $scope.syncing = false;
        // TODO: update context/standings
        $scope.game = data;
        $('#score-form').closeModal();
        // FIXME: players depend on game.post_save signal ... use websockets?!
        _loadPlayers();
      }).error(function(error) {
        $scope.syncing = false;
        _display_error(error);
      });
    };

    $scope.deleteGameModal = function(game) {
      $scope.game = game;
      $scope.openModal('#game-delete');
    };

    $scope.deleteGame = function() {
      if ($scope.syncing) {
        return;
      }
      $scope.syncing = true;

      var game_url = api_game_url + $scope.game.id + '/';

      $http.delete(game_url).success(function() {
        var socket_data = {
          'action': 'delete',
          'content_type': 'game',
          'id': $scope.game.id,
          'event_id': $scope.event.id
        };
        game_socket.send(JSON.stringify(socket_data));
        $scope.syncing = false;
        angular.forEach($scope.event.event_games, function(game_id, idx) {
          if (game_id === $scope.game.id) {
            $scope.event.event_games.splice(idx, 1);
          }
        });
        angular.forEach($scope.games, function(game, idx) {
          if (game.id === $scope.game.id) {
            $scope.games.splice(idx, 1);
          }
        });
        $scope.game = $scope.new_game;
        $scope.closeModal('#game-delete');

        // recalc numbers (this is already done server-side)
        $scope.games.sort(function(a, b) {
          return a.number - b.number;
        });
        angular.forEach($scope.games, function(game, idx) {
          game.number = idx + 1;
        });
      }).error(function(error) {
        $scope.syncing = false;
        _display_error(error);
      });
    };

    // game round
    $scope.generateGameRound = function() {
      $scope.new_games = [];
      var games;
      var scheduler = new Scheduler();
      if ($scope.modus === '1vs1') {
        games = scheduler.generate_1vs1($scope.event.players);
      } else if ($scope.modus === '2vs2') {
        games = scheduler.generate_2vs2($scope.event.players);
      }

      if ($scope.reverse) {
        games.reverse();
      }

      angular.forEach(games, function(game, idx) {
        var _home = (angular.isArray(game[0])) ? game[0] : [game[0]];
        var _away = (angular.isArray(game[1])) ? game[1] : [game[1]];

        var players_home = [];
        var players_away = [];

        angular.forEach(_home, function(obj) { players_home.push(obj.id); });
        angular.forEach(_away, function(obj) { players_away.push(obj.id); });

        var _game = {
          players_home: ($scope.mirrored) ? players_away : players_home,
          players_away: ($scope.mirrored) ? players_home : players_away,
          number: idx + 1,
          event: $scope.event.id
        };
        $scope.new_games.push(_game);
      });

      if ($scope.round_type === 'double') {
        // duplicate games with reverse order
        var _idx = $scope.new_games.length;
        angular.forEach($scope.new_games, function(game, idx) {
          var _game = {
            players_home: game.players_away,
            players_away: game.players_home,
            number: _idx + idx + 1,
            event: $scope.event.id
          };
          $scope.new_games.push(_game);
        });
      }
    };

    $scope.addGameRound = function() {
      // add $scope.new_games to event

      if ($scope.syncing) {
        return;
      }

      // get offset for number
      var offset = 0;
      angular.forEach($scope.games, function(game) {
        offset = Math.max(game.number, offset);
      });

      var post_data = [];
      angular.forEach($scope.new_games, function(game) {
        game.number += offset;
        post_data.push(game);
      });

      $scope.syncing = true;

      $http.post(api_game_url, post_data).success(function(data) {
        $scope.syncing = false;
        angular.forEach(data, function(game) { $scope.games.push(game); });
        $('#game-round').closeModal();
      }).error(function(error) {
        $scope.syncing = false;
        _display_error(error);
      });

    };

    $scope.scrollToNextGame = function() {
      // scroll to next game (status is not finished)
      var next;
      var games = $scope.games.slice(0).sort(function(a, b) {
        return a.number - b.number;
      });
      angular.forEach(games, function(game) {
        if (angular.isUndefined(next) && game.status !== 'finished') {
          next = game.number;
        }
      });
      next = next || games[games.length - 1].number;

      var $elem = $('#game-' + next);

      window.scrollTo(window.scrollX, $elem.offset().top);
      $elem.addClass('highlight');
      window.setTimeout(function() {
        $elem.removeClass('highlight');
      }, 3000);
    };
  });


// Filters


app.filter('moment', function() {
  return function(timestamp, format) {
    if (!timestamp) {
      return null;
    }
    if (format) {
      return moment(timestamp).format(format);
    }
    return moment(timestamp).fromNow();
  };
});

app.filter('get_player_objects', function() {
  return function(player_ids, player_list) {
    var players = [];
    angular.forEach(player_ids, function(player_id) {
      angular.forEach(player_list, function(player) {
        if (player.id === player_id) {
          players.push(player);
          return;
        }
      });
    });
    return players;
  };
});

app.filter('get_unassigned_players', function() {
  // filter players that are already assigned
  return function(players, game) {
    var _players = [];
    angular.forEach(players, function(player) {
      if (game.players_home.indexOf(player.id) === -1 &&
          game.players_away.indexOf(player.id) === -1) {
        _players.push(player);
      }
    });
    return _players;
  };
});

app.filter('get_datalist_value', function() {
  // return team name and (if enabled on event) team strength/power
  return function(team, power_factor_enabled) {
    var value = team.league.name + ': ' + team.name;
    if (power_factor_enabled) {
      value += ' (' + team.power + ')';
    }
    return value;
  };
});

app.filter('to_fixed', function() {
  return function(value, precision) {
    return parseFloat(value).toFixed(precision);
  };
});

app.filter('standings', function() {
  return function(players) {
    if (angular.isUndefined(players)) {
      return players;
    }

    // sort players for standings
    return players.sort(function(a, b) {
      if (a.points === b.points) {
        // check scores
        var a_scores = a.scores_for - a.scores_against;
        var b_scores = b.scores_for - b.scores_against;
        if (a_scores === b.scores) {
          // check scores_for
          if (a.scores_for === b.scores_for) {
            // check wins
            return b.wins - a.wins;
          }
          return b.scores_for - a.scores_for;
        }
        return b_scores - a_scores;
      }
      return b.points - a.points;
    });
  };
});

// helpers

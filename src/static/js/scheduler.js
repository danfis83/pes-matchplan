
var Scheduler;

Scheduler = function() {

  this.delimiter = '§';

  this.generate_1vs1 = function(object_list) {

    var i, j, game, l, new_idx;
    var games = [];
    var obj_list = [];
    var delimiter = this.delimiter;
    var game_rounds = 0;
    var _teams = [];
    var games_per_round = parseInt(object_list.length / 2, 10);
    for (i = 0; i < object_list.length; i++) { obj_list.push(i); }

    if (obj_list.length % 2 === 0) {
      // even
      //
      // 1 2  ->  1 3  ->  1 4
      // 3 4      4 2      2 3
      //
      // 1 2  ->  1 3  ->  1 5  ->  1 6  ->  1 4
      // 3 4      5 2      6 3      4 5      2 6
      // 5 6      6 4      4 2      2 3      3 5
      //
      game_rounds = obj_list.length - 1;
      _teams = obj_list.slice(0);
      for (i = 0; i < game_rounds; i++) {
        l = _teams.slice(0);
        for (j = 0; j < games_per_round; j++) {
          if (i % 2 === 1 && j === 0) {
            game = [l[1], l[0]];
          } else {
            game = [l[0], l[1]];
          }
          games.push(game);
          l = l.slice(2);
        }

        // spin the teams (clockwise)
        l = _teams.slice(0);
        for (j = 0; j < l.length; j++) {
          if (j === 0) {
            new_idx = j;
          } else if (j === 1) {
            new_idx = 3;
          } else if (j === 2) {
            new_idx = 1;
          } else if (j % 2 === 0) {
            new_idx = j - 2;
          } else if (j % 2 === 1) {
            new_idx = (j === l.length - 1) ? j - 1 : j + 2;
          }
          _teams[new_idx] = l[j];
        }
      }
    } else {
      // odd
      //
      // 1 2  ->  3 1  ->  5 3  ->  4 5  ->  2 4
      // 3 4      5 2      4 1      2 3      1 5
      // 5        4        2        1        3
      //
      game_rounds = obj_list.length;
      _teams = obj_list.slice(0);
      var _games = [];

      for (i = 0; i < game_rounds; i++) {
        l = _teams.slice(0);
        for (j = 0; j < games_per_round; j++) {
          game = l[0] + delimiter + l[1];
          _games.push(game);
          l = l.slice(2);
        }

        // spin the teams (clockwise)
        l = _teams.slice(0);
        for (j = 0; j < l.length; j++) {
          if (j % 2 === 0) {
            new_idx = (j === 0) ? 1 : j - 2;
          } else if (j % 2 === 1) {
            new_idx = (j === l.length - 2) ? j + 1 : j + 2;
          }
          _teams[new_idx] = l[j];
        }

      }

      // sorting/ordering
      var game_count = _games.length;
      var indexes = [];
      var _tmp = [];
      var k, game_split, index, _game, _team_idx, game_idx;

      var _sort = function(a, b) {
        return a.index - b.index;
      };

      for (i = 0; i < game_count; i++) {
        indexes = [];
        _tmp = [];
        for (j = 0; j < games.length; j++) {
          game_split = games[j].split(delimiter);
          for (k = 0; k < game_split.length; k++) {
            _tmp.push(game_split[k]);
          }
        }
        _tmp.reverse();

        for (j = 0; j < _games.length; j++) {
          _game = _games[j];
          index = -1;
          game_split = _game.split(delimiter);
          for (k = 0; k < game_split.length; k++) {
            l = _tmp.indexOf(game_split[k]);
            if (l > -1) {
              _team_idx = l;
              if (_team_idx % 2 === 1) {
                _team_idx--;
              }
              index -= _team_idx;
            } else {
              index -= _teams.length;
            }
          }

          indexes.push({game: j, index: index});
        }

        indexes.sort(_sort);

        game_idx = indexes[0].game;
        games.push(_games[game_idx]);
        _games.splice(game_idx, 1);
      }

      _games = games;
      games = [];
      _games.forEach(function(_game) {
        games.push(_game.split(delimiter));
      });

    }

    // reassign indexes to object_list
    var result = [];
    games.forEach(function(game) {
      result.push([object_list[game[0]], object_list[game[1]]]);
    });

    return result;
  };

  this.generate_2vs2 = function(object_list) {

    var i, j;
    var obj_list = [];
    var games = [];
    var delimiter = this.delimiter;
    for (i = 0; i < object_list.length; i++) { obj_list.push(i); }

    var count = function(arr, itm) {
      var _result = 0;
      arr.forEach(function(obj) {
        if (JSON.stringify(obj) === JSON.stringify(itm)) {
          _result++;
        }
      });
      return _result;
    };

    var _teams = [];
    obj_list.forEach(function(_obj_i) {
      obj_list.forEach(function(_obj_j) {
        var _team = [_obj_i, _obj_j].sort();
        if (_obj_i !== _obj_j && count(_teams, _team) === 0) {
          _teams.push(_team);
        }
      });
    });

    var _games = [];
    _teams.forEach(function(_team_i) {
      _teams.forEach(function(_team_j) {
        if (_team_i !== _team_j) {
          if (_team_j.indexOf(_team_i[0]) === -1 &&
              _team_j.indexOf(_team_i[1]) === -1) {
            var pair1 = [_team_i, _team_j].sort();
            var pair2 = [_team_j, _team_i].sort();
            if (count(_games, pair1) === 0 && count(_games, pair2) === 0) {
              _games.push(pair1);
            }
          }
        }
      });
    });

    // sorting/ordering
    var _todo, _done, _tmp, _home, _away, _teams_count, game_idx;
    var number_of_games = _games.length;
    var count_factor = 2;

    var _get_sort_index = function(item) {

      var idx = 0;

      var _item1 = item[0];
      var _item2 = item[1];

      // count objects
      idx += count(_tmp, _item1[0]) * count_factor;
      idx += count(_tmp, _item1[1]) * count_factor;
      idx += count(_tmp, _item2[0]) * count_factor;
      idx += count(_tmp, _item2[1]) * count_factor;

      // teams count
      idx += _teams_count[_item1[0] + delimiter + _item1[1]];
      idx += _teams_count[_item2[0] + delimiter + _item2[1]];

      return idx;

    };

    var _sort = function(a, b) {
      var idx = _get_sort_index(a) - _get_sort_index(b);

      // NOTE: this is weird, but apparently the comparison for 0 is not
      // string based per default (which will result in "illogical" results)
      if (idx === 0 && JSON.stringify(a) < JSON.stringify(b)) {
        idx = -1;
      } else if (idx === 0 && JSON.stringify(a) > JSON.stringify(b)) {
        idx = 1;
      }

      return idx;
    };

    for (i = 0; i < number_of_games; i++) {

      _todo = _games.slice(0);
      _done = games.slice(0);
      _tmp = [];
      for (j = 0; j < _done.length; j++) {
        _home = _done[j][0];
        _away = _done[j][1];
        _tmp.push(_home[0]);
        _tmp.push(_home[1]);
        _tmp.push(_away[0]);
        _tmp.push(_away[1]);
      }

      _teams_count = {};
      for (j = 0; j < _teams.length; j++) {
        _teams_count[_teams[j][0] + delimiter + _teams[j][1]] = 0;
      }
      for (j = 0; j < _done.length; j++) {
        _teams_count[_done[j][0][0] + delimiter + _done[j][0][1]] += 1;
        _teams_count[_done[j][1][0] + delimiter + _done[j][1][1]] += 1;
      }

      _todo.sort(_sort);

      game_idx = _games.indexOf(_todo[0]);
      games.push(_games[game_idx]);
      _games.splice(game_idx, 1);

    }

    var _game;
    var result = [];
    games.forEach(function(_g, idx) {
      _home = _g[0];
      _away = _g[1];
      _home = [object_list[_home[0]], object_list[_home[1]]];
      _away = [object_list[_away[0]], object_list[_away[1]]];
      _game = [_home, _away];

      // FIXME: optimize this (if possible)
      if (idx > 0 && (idx % 2 === 0 || idx % object_list.length - 1 === 1)) {
        _game.reverse();
      }

      result.push(_game);
    });

    return result;

  };

};

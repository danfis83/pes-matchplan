# PES Matchplan #

An app to track PES events/tournaments.

## Setup ##

1. Clone or Download Repository
2. Install dependencies ( ```pip install -r requirements.txt```)
3. Setup Database (django signal support required!)
4. Add Environment Variables:

    * ```DATABASE_URL```
    * ```DJANGO_SECRET_KEY```

5. Run migrate: ```src/manage.py migrate```
6. instead of `src/manage.py runserver` use `src/tornado_server.py [--autoreload]` (for sockets)

## TODOS ##

* API

* Frontend

    * Dashboard
    * Events
    * Players
    * Account-Management